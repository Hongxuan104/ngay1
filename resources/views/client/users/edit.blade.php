@extends('layouts.master')

@section('page_title')
    User
@endsection

@section('main')
@if($errors->any())
<div class="alert alert-danger">Dữ liệu nhâp vào không hợp lệ</div>
@endif
<form action="" method="POST">
    <div class="mb-3">
        <label for="">Họ và tên</label><br>
        <input type="text" class="form-control" name="fullname" placeholder="Nhập họ và tên..." value="{{old('fullname') ?? $userDetail->fullname}}">
        @error('fullname')
        <span style="color:red">{{$message}}</span>
        @enderror
    </div>
    <div class="mb-3">
        <label for="">Email</label><br>
        <input type="text" class="form-control" name="email" placeholder="Nhập email..." value="{{old('email') ?? $userDetail->email}}">
        @error('email')
        <span style="color:red">{{$message}}</span>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Cập nhật</button>
    <a href="{{ route('users.index') }}" class="btn btn-primary">Quay lại</a>
    @csrf
</form>
@endsection