<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct()
    {
        
    }

    //Hiển thị danh sách
    public function index()
    {
        return 'Danh sách chuyên mục ';
    }

    //Lấy ra 1 chuyên mục theo id
    public function getCategory($id)
    {
        return 'Chi tiết chuyên mục'.$id;
    }

    //Cập nhật (POST)
    public function updateCategory($id)
    {
        
    }

    //Show form thêm dữ liệu
    public function showCategory()
    {

    }

    //Thêm dữ liệu vào chuyên mục (POST)
    public function addCategory($id)
    {
        return 'Form thêm chuyên mục';
    }

    //Xóa dữ liệu
    public function deleteCategory($id)
    {

    }

    public function handleCategory()
    {

    }
}
