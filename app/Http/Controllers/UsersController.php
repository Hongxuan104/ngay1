<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    protected $users;

    const _PER_PAGE = 4;
    public function __construct()
    {
        $this->users = new Users();

    }

    public function index()
    {
    $title = 'Danh sách người dùng';

    //$this->users->learnQueryBuilder();
    
    $users = new Users();

    $usersList = $users->getAllUsers(self::_PER_PAGE);

    return view('client.users.list', compact('title', 'usersList'));



    }

    public function add()
    {
        $title = 'Thêm người dùng';

        return view('client.users.add', compact('title'));
    }

    public function postAdd(Request $request)
    {
        $request->validate([
            'fullname' => 'required|min:5',
            'email' => 'required|email|unique:users'
        ], [
            'fullname.required' => 'Họ và tên bắt buộc phải nhập',
            'fullname.min' => 'Họ và tên phải từ :min kí tự trở lên',
            'email.required' => 'Email bắt buộc phải nhập',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã tồn tại trên hệ thống'
        ]);

        $dataInsert = [ 
            $request->fullname,
            $request->email,
            date('Y-m-d H:i:s') 
        ];
        $this->users->addUser($dataInsert);

        return redirect()->route('users.index')->with('Thêm người dùng thành công');
    }

    public function getEdit($id=0) {
        $title = 'Cập nhật người dùng người dùng';

        if(!empty($id)){
            $userDetail = $this->users->getDetail($id);
            if(!empty($userDetail[0])){
                $userDetail = $userDetail[0];
            }else{
                return redirect()->route('users.index');
            }
        }else{
            return redirect()->route('users.index');
        }

        return view('client.users.edit', compact('title', 'userDetail'));
    }
    public function postEdit(Request $request, $id){
        $request->validate([
            'fullname' => 'required|min:5',
            'email' => 'required|email',
        ], [
            'fullname.required' => 'Họ và tên bắt buộc phải nhập',
            'fullname.min' => 'Họ và tên phải từ :min kí tự trở lên',
            'email.required' => 'Email bắt buộc phải nhập',
            'email.email' => 'Email không đúng định dạng',
          //  'email.unique' => 'Email đã tồn tại trên hệ thống'
        ]);

        $dataUpdate = [ 
            $request->fullname,
            $request->email,
            date('Y-m-d H:i:s') 
        ];
        $this->users->updateUser($dataUpdate, $id);

        return redirect()->route('users.index', ['id'=>$id]);
    }

    public function delete($id = 0){
        if(!empty($id)){
            $userDetail = $this->users->getDetail($id);
            if(!empty($userDetail[0])){
                $deleteStatus = $this->users->deleteUser($id);
                if($deleteStatus){
                    $msg = 'Xóa thành công'; 
                }else{
                    $msg = 'Bạn không thể xóa';
                }
            }else{
                return redirect()->route('users.index');
            }
        }else{
            return redirect()->route('users.index');
        }
        return redirect()->route('users.index');
    }

    /* public function pagination()
    {
        return view('users.index', [
            'users' => DB::table('users')->paginate(15)
        ]);
    } */
}
