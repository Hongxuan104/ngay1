<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    use HasFactory;

    protected $table = 'post';
    public $timestamps = false;
    protected $fillable = [
        'fullname',
        'email',
        'group_id',
        'update_at',
        'create_at',
    ];
    public function getAllUsers($perPage = null)
    {
        //$users = DB::select('SELECT * FROM users ORDER BY create_at DESC');
        $users = DB::table($this->table);

        if(!empty($perPage)){
            $users = $users->paginate($perPage);
        }else{
            $users = $users->get();
        }

        return $users;
    }
    
    public function addUser($data){
        DB::insert('INSERT INTO users (fullname, email, create_at, update_at, group_id) value (?, ?, ?, ?, ?)', $data);
    }

    public function getDetail($id){
       return DB::select('SELECT * FROM '.$this->table.' WHERE id = ?', [$id]);
    }

    public function updateUser($data, $id)
    {
        $data[] = $id;
        return DB::update('UPDATE '.$this->table.' SET fullname=?, email=?, update_at=? where id = ?', $data);
    }

    public function deleteUser($id){
        return DB::delete("DELETE FROM $this->table WHERE id=?", [$id]);
    }

    public function learnQueryBuilder()
    {
        DB::enableQueryLog();
        //Lấy tất cả bản ghi
        $id = 20;
        $lists = DB::table($this->table)
        ->select('fullname as hoten', 'email', 'id') 
          ->where('id', 19)
        ->where(function($query) use ($id){
            $query->where('id', '<', $id)->orWhere('id', '>', $id);
        }); 
        //->where('fullname', 'like', '%Hong%')
        //->whereBetween('id', [9, 10])
        //->whereIn('id', [8, 10])
        //->whereMonth('create_at', '03')

        //->get();
        //->toSql();

        //Join bảng
        //$lists = DB::table('users')
        //->select('users.*', 'groups.name as group_name')
        //->join('groups', 'users.group_id', '=', 'groups.id')
        //->select(DB::raw('count(id) as group_id'), 'email')
        //->groupBy('email')
        //->having('group_id', '>=', 2)
        //->get();

        //dd($lists);
        // $status = DB::table('users')->insert([
        //     'fullname' => 'Hong1246',
        //     'email' => 'nguyenvana@gmail.com',
        //    'group_id' => 1,
        //    'create_at' => date('Y-m-d H:i:s'),
        //]);
        //dd($status);
        //$lastId = DB::getPdo()->lastInsertId();
        //dd($lastId);

        $status = DB::table('users')
        ->where('id',17)
        ->update([
            'fullname' => 'Hong999',
            'email' => 'nguyenvana@gmail.com',
            'update_at' => date('Y-m-d H:i:s'),
        ]); 

         $status = DB::table('users')
        ->where('id', 13)
        ->delete(); 

        //$lists = DB::table('users')->where('id', '>', 20)->get();
        //$count = count($lists);
        //dd($count);

        $lists = DB::table('users')
        ->selectRaw('fullname, email')
        //->groupBy('email')
        ->orderByRaw('create_at DESC, update_at ASC')
        ->get();
        $sql = DB::getQueryLog();
        dd($sql);

        //Lấy bản ghi đầu tiên
        $detail = DB::table($this->table)->first();
    } 
}
