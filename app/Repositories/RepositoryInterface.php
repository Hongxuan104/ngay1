<?php

namespace App\Repositories;

interface RepositoryInterface
{
    /**
    * Get All
    * @return mixed
    */
    public function getAll($attributes = []);
    
    /**
    * Get One
    * @param $id
    * @return mixed
    */
    public function find($id);
    
    /**
    * Create
    * @param array $attribute
    * @return mixed
    */
    public function create($attribute = []);
    
    /**
    * Update
    * @param $id
    * @param array $attribute
    * @return mixed
    */
    public function update($id, $attribute = []);
    
    /**
    * Delete
    * @param $id
    * @return mixed
    */
    public function delete($id);
}