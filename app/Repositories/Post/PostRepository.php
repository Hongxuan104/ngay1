<?php
namespace App\Repositories\Post;

use App\Repositories\BaseRepository;

class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    /**
     * Get Model Post
     */
    public function getModel()
    {
        return \App\Models\Post::class;
    }

    public function getUsers()
    {
        return $this->model->select('users')->get();
    }
    public function getAll($attributes = [])
    {
        if(isset($attributes['title']))
        {
            return $this->model->where('title', 'like', '%'.$attributes['title'].'%')->get();
        }
        
        return $this->model->get();
    }
}
